<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>


<html>

<body>
	<sec:authorize access="isAuthenticated()">
		<form class="form-inline"  method="post" role="logout"
			action="<c:url value="/j_spring_security_logout" />">
			<button type="submit" class="btn btn-success">Выход!</button>
		</form>
	</sec:authorize>
	<sec:authorize access="not isAuthenticated()">
		<form class="form-inline"  method="post" role="login"
			action="<c:url value="/j_spring_security_check" />">

			<div class="form-group">
				<label for="inputLogin" class="sr-only">Логин</label>
				
					<input type="text" class="form-control" id="inputLogin" name="j_username"
						placeholder="Ведите логин">
				
			</div>

			<div class="form-group">
				<label for="inputPassword" class="sr-only">Пароль</label>
				
					<input type="password" class="form-control" id="inputPassword"
					 name="j_password"	placeholder="Введите пароль">
				
			</div>

			<div class="form-group">
				
					<input type="submit" class="btn btn-success" value="Заходим!"/>
				
			</div>

			<div class="form-group">

				<div class="col-sm-3">
					<a href="registration">Регистрируемся!</a>
				</div>
			</div>
			
		</form>
	</sec:authorize>


</body>
</html>