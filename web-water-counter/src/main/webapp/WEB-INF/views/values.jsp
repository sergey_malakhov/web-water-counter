<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>СУППУ-Ваши счетчики</title>
<link
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />"
	rel="stylesheet">
</head>
<body>


	<div class="container">

		<div class="row">
			<div class="col-sm-offset-2 col-sm-10 text-right">
				<jsp:include page="/WEB-INF/views/auth.jsp"></jsp:include>
			</div>
		</div>

		<div class="row">

			<div class="col-sm-3">
				<nav>
					<ul class="nav nav-pills nav-stacked">
						<li><a href="home">Моя страница</a></li>
						<li class="active"><a href="#">Показания</a></li>
						<li><a href="counters">Информация по счетчикам</a></li>
						<li><a href="#">Информация о пользователе</a></li>
						<li><a href="#">Информация о квартире</a></li>
					</ul>
				</nav>
			</div>

			<div class="col-sm-9">
				<h3>Показания ваших счетчиков</h3>

				<c:choose>
					<c:when test="${counters.isEmpty()}">
						<p class="text-warning">
							У вас пока нет ни одного водосчетчика.&nbsp;<a
								href="counters/add">Добавить?</a>
					</c:when>
					<c:otherwise>
						<table class="table table-bordered table-hover">
							<tbody>
								<tr>
									<th colspan="1"></th>
									<th class="text-muted">Показания за прошлый месяц ( на dd.MM.yyyy)</th>
									<th class="text-muted">Показания за текущий месяц ( на exists ? dd.MM.yyyy : toDay)</th>
								</tr>
								<tr>
									<th colspan="3" class="text-muted">Счетчики в ванной</th>
									
								</tr>
								<tr>
									<th>
										<div class="text-primary" style="margin-left: 20px">Счетчик холодной воды №&nbsp;12343</div>
									</th>									
									<td>000568</td>
									<td>000572</td>
								</tr>
								<tr>
									<th>
										<div class="text-danger" style="margin-left: 20px">Счетчик горячей воды №&nbsp;03478</div>
									</th>
									<td>000398</td>
									<td>000404</td>
								</tr>
								<tr>
									<th colspan="3" class="text-muted">Счетчики на кухне</th>
									
								</tr>
								<tr>
									<th>
										<div class="text-primary" style="margin-left: 20px">Счетчик холодной воды №&nbsp;49033</div>
									</th>
									
									<td>000458</td>
									<td>000462</td>
								</tr>
								<tr>
									<th>
										<div class="text-danger" style="margin-left: 20px">Счетчик горячей воды №&nbsp;03421</div>
									</th>
									
									<td>000502</td>
									<td>000508</td>
								</tr>
								
							</tbody>
						</table>
						<p class="text-right">
							<a href="values/edit" class="btn btn-success">Изменить?</a>							
						</p>
					</c:otherwise>

				</c:choose>

			</div>

		</div>



	</div>



	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://code.jquery.com/jquery.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script
		src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>

</body>
</html>