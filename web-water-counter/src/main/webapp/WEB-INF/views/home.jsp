<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">
<title>СУППУ-Ваша страница</title>
</head>
<body>
	<div class="container">

		<div class="row">
			<div class="col-sm-offset-2 col-sm-10 text-right">
				<jsp:include page="/WEB-INF/views/auth.jsp"></jsp:include>
			</div>
		</div>

		<div class="row">

			<div class="col-sm-3">
				<nav>
					<ul class="nav nav-pills nav-stacked">
						<li class="active"><a href="home">Моя страница</a></li>
						<li><a href="values">Показания</a></li>
						<li><a href="counters">Информация по счетчикам</a></li>
						<li><a href="#">Информация о пользователе</a></li>
						<li><a href="#">Информация о квартире</a></li>
					</ul>
				</nav>
			</div>
			<div class="col-sm-9">
				<h3>
					Добро пожаловать <c:out value="${userName}"/>!
				</h3>
				<p class="text-warning">Вы не добавили ни одного счетчика. <a href="#">Добавить?</a></p>
				<p class="text-warning">Вы не указали информацию о помещении. 
				Без этой информации будет невозможно сформировать квитанцию о потреблении ресурсов за отчетный период. <a href="#">Указать?</a></p>
				
			</div>

		</div>

	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://code.jquery.com/jquery.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>