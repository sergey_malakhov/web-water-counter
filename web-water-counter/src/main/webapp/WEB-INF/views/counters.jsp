<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>СУППУ-Ваши счетчики</title>
<link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>


	<div class="container">

		<div class="row">
			<div class="col-sm-offset-2 col-sm-10 text-right">
				<jsp:include page="/WEB-INF/views/auth.jsp"></jsp:include>
			</div>
		</div>

		<div class="row">

			<div class="col-sm-3">
				<nav>
					<ul class="nav nav-pills nav-stacked">
						<li><a href="home">Моя страница</a></li>
						<li><a href="values">Показания</a></li>
						<li class="active"><a href="counters">Информация по
								счетчикам</a></li>
						<li><a href="#">Информация о пользователе</a></li>
						<li><a href="#">Информация о квартире</a></li>
					</ul>
				</nav>
			</div>

			<div class="col-sm-9">
				<h3>Ваши счетчики</h3>

				<c:choose>
					<c:when test="${coldCounters.isEmpty() && hotCounters.isEmpty()}">
						<p class="text-warning">
							У вас пока нет ни одного водосчетчика.&nbsp;<a
								href="counters/add">Добавить?</a>
					</c:when>
					<c:otherwise>
						<table class="table table-bordered table-hover">
							<tbody>
								<tr>
									<th colspan="4">Cчетчики холодной воды</th>
								</tr>
								<tr>
									<th>Номер</th>
									<th>Тип</th>
									<th>Расположение</th>
									<th />
								</tr>
								<c:forEach items="${coldCounters}" var="item">
									<tr>
										<td>${item.pasportNumber}</td>
										<td>${item.type.title}</td>
										<td>${item.place.title}</td>
										<td class="text-center">
											<%-- <a href="counters/view/${item.id}"
											class="glyphicon glyphicon-search text-success" title="Посмотреть"></a> --%>
											<a href="counters/edit/${item.id}"
											class="glyphicon glyphicon-pencil" title="Редактировать"></a>
										<a
											href="counters/delete/${item.id}"
											class="glyphicon glyphicon-remove text-danger"
											title="Удалить"></a> 
											</td>
									</tr>
								</c:forEach>
								<tr>
									<th colspan="4">Счетчики горячей воды</th>
								</tr>
								<tr>
									<th>Номер</th>
									<th>Тип</th>
									<th>Расположение</th>
									<th />
								</tr>
								<c:forEach items="${hotCounters}" var="item">
									<tr>
										<td>${item.pasportNumber}</td>
										<td>${item.type.title}</td>
										<td>${item.place.title}</td>
										<td class="text-center">
											<%-- <a href="counters/view/${item.id}"
											class="glyphicon glyphicon-search text-success" title="Посмотреть"></a> --%>
											<a href="counters/edit/${item.id}"
											class="glyphicon glyphicon-pencil" title="Редактировать"></a>
											<a href="counters/delete/${item.id}"
											class="glyphicon glyphicon-remove text-danger"
											title="Удалить"></a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<p class="text-right">
							<a href="counters/add" class="btn btn-success">Добавить еще?</a>

						</p>
					</c:otherwise>

				</c:choose>

			</div>

		</div>



	</div>



	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://code.jquery.com/jquery.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>

</body>
</html>