<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>СУППУ</title>

<link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">

</head>
<body>
	<div class="container">

		
		

		<h1 class="text-center">Система учета показаний приборов учета</h1>
		<p class="lead">Если в вашей квартире установлены индивидуальные
			приборы учета, такие как водосчетчики, теплосчетчики, газосчетчики,
			то вы обязаны ежемесячно снимать с них показания и передавать их в
			Вашу управляющую компанию. Этот сервис поможет вам вести учет
			потребляемых ресурсов, сохранить значения показаний за прошлые
			отчетные периоды.</p>
		<div class="row text-center">
				<jsp:include page="/WEB-INF/views/auth.jsp"></jsp:include>
			<!-- <div class="col-sm-offset-2 col-sm-10 text-right">				
			</div> -->
		</div>

		
		<!-- <div class="row">
			<div class="col-md-12 text-center">

				<h3>Последние новости</h3>

			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<h4>Новость 04.01.2014</h4>
				<p>
					Вывожу из запоя! Потомственный нарколог окажет анонимную помощь.<a
						href="#">Подробнее...</a>
				</p>
			</div>
			<div class="col-md-3 ">
				<h4>Новость 03.01.2014</h4>
				<p>
					Сегодня уже 3 января<a href="#">...</a>
				</p>
			</div>
			<div class="col-md-3 ">
				<h4>Новость 02.01.2014</h4>
				<p>
					Новый год был вчера<a href="#">...</a>
				</p>
			</div>
			<div class="col-md-3 ">
				<h4>Новость 01.01.2014</h4>
				<p>
					Сновым 2014 годом!<a href="#">...</a>
				</p>
			</div>
		</div>

		<div class="footer">
			<p>© Company 2013</p>
		</div>
 -->
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://code.jquery.com/jquery.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>