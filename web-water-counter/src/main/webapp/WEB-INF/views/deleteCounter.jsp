<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>СУППУ-Удаление счетчика</title>
<link
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />"
	rel="stylesheet">

</head>
<body>

	<div class="container">

		<div class="row">
			<div class="col-sm-offset-2 col-sm-10 text-right">
				<jsp:include page="/WEB-INF/views/auth.jsp"></jsp:include>
			</div>
		</div>

		<div class="row">

			<div class="col-sm-3">
				<nav>
					<ul class="nav nav-pills nav-stacked">
						<li><a href="#">Показания</a></li>
						<li><a href="<c:url value="/counters" />">Информация по
								счетчикам</a></li>
						<li><a href="#">Информация о пользователе</a></li>
						<li><a href="#">Информация о квартире</a></li>
					</ul>
				</nav>
			</div>

			<div class="col-sm-9">
				<h3>Удаление счетчика</h3>

				<form:form method="POST" commandName="counter"
					action="${counter.id}" cssClass="form-horizontal"
					role="deleteCounter">

					<table class="table table-bordered table-hover">

						<tbody>

							<tr>
								<th>Номер</th>
								<td><c:out value="${counter.pasportNumber}" /></td>
							</tr>
							<tr>
								<th>Тип</th>
								<td><c:out value="${counter.type.title}" /></td>
							</tr>
							<tr>
								<th>Расположение</th>
								<td><c:out value="${counter.place.title}" /></td>
							</tr>
					</table>

					

					<p class="text-right">
						<input type="submit" value="Удалить!" class="btn btn-danger" />
					</p>

				</form:form>

			</div>

		</div>



		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://code.jquery.com/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script
			src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />">
			
		</script>


	</div>

</body>
</html>