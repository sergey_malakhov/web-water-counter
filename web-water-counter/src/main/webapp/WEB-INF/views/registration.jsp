<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>СУППУ-Регистрация</title>

<link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>

	<div class="container">

		<div class="row">
			<div class="col-sm-offset-2 col-sm-10 text-right">
				<jsp:include page="/WEB-INF/views/auth.jsp"></jsp:include>
			</div>
		</div>

		<div class="row">
			<h2>Ругистрация нового пользователя</h2>
		</div>

		<form:form method="POST" commandName="user" action="registration"
			cssClass="form-horizontal" role="registration">

			<div class="form-group">
				<label for="login" class="col-sm-4 control-label">Логин</label>
				<div class="col-sm-4">

					<form:input id="login" cssClass="form-control" path="login"
						placeholder="Введите логин" />
				</div>
				<div class="col-sm-4">

					<form:errors path="login" cssClass="text-danger" />
				</div>
			</div>

			<div class="form-group">
				<label for="email" class="col-sm-4 control-label">Адрес
					электронной почты (email)</label>
				<div class="col-sm-4">
					<form:input id="email" cssClass="form-control" path="email"
						placeholder="Введите email" />

				</div>
				<div class="col-sm-4">
					<form:errors path="email" cssClass="text-danger" />

				</div>
			</div>

			<div class="form-group">
				<label for="email" class="col-sm-4 control-label">Пароль</label>
				<div class="col-sm-4">
					<form:password path="pwdHash" cssClass="form-control"
						placeholder="Введите пароль" />

				</div>
				<div class="col-sm-4">
					<form:errors path="pwdHash" cssClass="text-danger" />

				</div>
			</div>
			<div class="col-sm-offset-4 col-sm-4">
				<input type="submit" value="Зарегистрироватся!"
					class="btn btn-success" title="Зарегистрироватся!" />

			</div>

		</form:form>

	</div>

	<div class="footer">
		<p>© Company 2013</p>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://code.jquery.com/jquery.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>