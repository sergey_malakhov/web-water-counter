<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>СУППУ-Новый счетчик</title>
<link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>

	<div class="container">

		<div class="row">
			<div class="col-sm-offset-2 col-sm-10 text-right">
				<jsp:include page="/WEB-INF/views/auth.jsp"></jsp:include>
			</div>
		</div>

		<div class="row">

			<div class="col-sm-3">
				<nav>
					<ul class="nav nav-pills nav-stacked">
						<li><a href="#">Показания</a></li>
						<li class="active"><a href="../counters">Информация по
								счетчикам</a></li>
						<li><a href="#">Информация о пользователе</a></li>
						<li><a href="#">Информация о квартире</a></li>
					</ul>
				</nav>
			</div>

			<div class="col-sm-9">
				<h3>Новый счетчик</h3>

				<form:form method="POST" commandName="counter" action="add"
					cssClass="form-horizontal" role="newCounter">

					<div class="form-group">
						<label for="number" class="col-sm-2  control-label">Номер</label>
						<div class="col-sm-5">

							<form:input id="number" cssClass="form-control"
								path="pasportNumber" placeholder="Введите номер счетчика" />
						</div>
						<div class="col-sm-5">

							<form:errors path="pasportNumber" cssClass="text-danger" />
						</div>
					</div>

					<div class="form-group">
						<label for="type" class="col-sm-2  control-label">Тип</label>
						<div class="col-sm-5">

							<form:select items="${counterTypes}" itemLabel="title" path="type"
								cssClass="form-control " >
							</form:select>
						</div>
						<div class="col-sm-5">

							<form:errors path="type" cssClass="text-danger" />
						</div>
					</div>

					<div class="form-group">
						<label for="place" class="col-sm-2  control-label">Расположение</label>
						<div class="col-sm-5">

							<form:select items="${counterPlaces}" itemLabel="title" path="place"
								cssClass="form-control " />
						</div>
						<div class="col-sm-5">

							<form:errors path="place" cssClass="text-danger" />
						</div>
					</div>

					<div class="col-sm-offset-2 col-sm-5">
						<div class="text-right">
						<input type="submit" value="Добавить!"
							class="btn btn-success" />
						
						</div>

					</div>

					<%-- <table>
						<tr>
							<td>Номер</td>
							<td><form:input path="pasportNumber" /></td>
							<td><form:errors path="pasportNumber" /></td>
						</tr>

						<tr>
							<td>Тип</td>
							<td><form:select items="${counterTypes}" itemLabel="title"
									path="type" /></td>
							<td><form:errors path="type" /></td>
						</tr>
						<tr>
							<td>Описание</td>
							<td><form:input path="placeDescription" /></td>
							<td><form:errors path="type" /></td>
						</tr>

						<tr>
							<td colspan="3"><input type="submit" value="Создать!"
								title="Создать!" /></td>
						</tr>
					</table> --%>
				</form:form>

			</div>

		</div>



		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://code.jquery.com/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>

	</div>

	<%-- <h1>Система учета показаний приборов учета</h1>
	<h2>Ругистрация нового пользователя</h2>

	<form:form method="POST" commandName="counter" action="add">

		<table>
			<tr>
				<td>Номер</td>
				<td><form:input path="pasportNumber" /></td>
				<td><form:errors path="pasportNumber" /></td>
			</tr>

			<tr>
				<td>Тип</td>
				<td><form:select items="${counterTypes}" itemLabel="title"
						path="type" /></td>
				<td><form:errors path="type" /></td>
			</tr>
			<tr>
				<td>Описание</td>
				<td><form:input path="placeDescription" /></td>
				<td><form:errors path="type" /></td>
			</tr>

			<tr>
				<td colspan="3"><input type="submit" value="Создать!"
					title="Создать!" /></td>
			</tr>
		</table>
	</form:form> --%>


</body>
</html>