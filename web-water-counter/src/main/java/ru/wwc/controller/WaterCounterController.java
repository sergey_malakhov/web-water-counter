package ru.wwc.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.wwc.domain.WaterCounter;
import ru.wwc.domain.WaterCounterPlase;
import ru.wwc.domain.WaterCounterType;
import ru.wwc.service.UserService;
import ru.wwc.service.WaterCounterService;

@Controller
@RequestMapping("/counters")
public class WaterCounterController {
	
	@Autowired
	WaterCounterService service;
	
	@Autowired
	UserService userService;
	
	@RequestMapping(method=RequestMethod.GET)
	public String getCountersInfo(ModelMap model) {
		
		Authentication  auth = SecurityContextHolder.getContext().getAuthentication();
		String userLogin = auth.getName();
		
		List<WaterCounter> allCounters = service.getUserCounters(userLogin);
		List<WaterCounter> hotCounters = new ArrayList<WaterCounter>();
		List<WaterCounter> coldCounters = new ArrayList<WaterCounter>();
		
		for(WaterCounter counter : allCounters) {
			switch(counter.getType()) {
				case COLD:
					coldCounters.add(counter);
					break;
				case HOT:
					hotCounters.add(counter);
					break;
				default:
					break;
			}
		}		
		
		model.addAttribute("coldCounters", coldCounters);
		model.addAttribute("hotCounters", hotCounters);
		
		return "counters";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/add")
	public String createCounter(ModelMap model) {
		model.addAttribute("counter", new WaterCounter());
		model.addAttribute("counterTypes", Arrays.asList(WaterCounterType.values()));
		model.addAttribute("counterPlaces", Arrays.asList(WaterCounterPlase.values()));
		return "addCounter";
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/add")
	public String doCreateCounter(@Valid @ModelAttribute("counter") WaterCounter counter, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			model.addAttribute("counterTypes", Arrays.asList(WaterCounterType.values()));
			model.addAttribute("counterPlaces", Arrays.asList(WaterCounterPlase.values()));
			return "addCounter";
		}
		
		Authentication  auth = SecurityContextHolder.getContext().getAuthentication();
		String userLogin = auth.getName();

		service.create(counter, userLogin);

		return "redirect:/counters";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/edit/{id}")
	public String editCounter(@PathVariable Long id, ModelMap model) {
		Authentication  auth = SecurityContextHolder.getContext().getAuthentication();
		String userLogin = auth.getName();
		
		List<WaterCounter> userCounters = service.getUserCounters(userLogin);
		WaterCounter currentCounter = null;
		for(WaterCounter counter : userCounters) {
			if(counter.getId().equals(id)) {
				currentCounter = counter;
				break;
			}
		}
		if(currentCounter == null) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST).toString();
		} 
		model.addAttribute("counter", currentCounter);
		model.addAttribute("counterTypes", Arrays.asList(WaterCounterType.values()));
		model.addAttribute("counterPlaces", Arrays.asList(WaterCounterPlase.values()));
		
		return "editCounter";
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/edit/{id}")
	public String doEditCounter(@Valid @ModelAttribute("counter") WaterCounter counter, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			model.addAttribute("counterTypes", Arrays.asList(WaterCounterType.values()));
			model.addAttribute("counterPlaces", Arrays.asList(WaterCounterPlase.values()));
			return "editCounter";
		}
		
		Authentication  auth = SecurityContextHolder.getContext().getAuthentication();
		String userLogin = auth.getName();
		
		
		service.update(counter, userLogin);

		return "redirect:/counters";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/delete/{id}")
	public String removeCounter(@PathVariable Long id, ModelMap model) {
		Authentication  auth = SecurityContextHolder.getContext().getAuthentication();
		String userLogin = auth.getName();
		
		List<WaterCounter> userCounters = service.getUserCounters(userLogin);
		WaterCounter currentCounter = null;
		for(WaterCounter counter : userCounters) {
			if(counter.getId().equals(id)) {
				currentCounter = counter;
				break;
			}
		}
		if(currentCounter == null) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST).toString();
		} 
		model.addAttribute("counter", currentCounter);

		
		return "deleteCounter";
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/delete/{id}")
	public String doRemoveCounter(@PathVariable Long id, ModelMap model) {
		
		
		Authentication  auth = SecurityContextHolder.getContext().getAuthentication();
		String userLogin = auth.getName();
		
		
		service.delete(id, userLogin);

		return "redirect:/counters";
	}
	
}
