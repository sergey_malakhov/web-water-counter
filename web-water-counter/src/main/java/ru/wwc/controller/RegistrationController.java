package ru.wwc.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.wwc.dao.RoleDao;
import ru.wwc.dao.UserDao;
import ru.wwc.domain.User;
import ru.wwc.exception.domain.user.UserException;
import ru.wwc.service.UserService;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

	@Autowired
	private UserDao userDao;

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET)
	public String doRegistration(ModelMap model) {
		model.addAttribute("user", new User());
		return "registration";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String doRegistrationAndAuth(@Valid User user, BindingResult result) {

		if (result.hasErrors()) {
			return "registration";
		}

		try {
			User newUser = userService.createUser(user);
			userService.authorizeUser(newUser);
		} catch (UserException e) {
			if(e.getErrorKeys().contains(UserException.NON_UNIQUE_LOGIN)) {
				result.rejectValue("login", "user.nonUniqueLogin");				
			}
			if(e.getErrorKeys().contains(UserException.NON_UNIQUE_EMAIL)) {
				result.rejectValue("email", "user.nonUniqueEmail");			
			}
			return "registration";
		} 

		return "redirect:/home";
	}
}
