package ru.wwc.controller;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller()
@RequestMapping("/")
public class MainController {
	@RequestMapping(method = RequestMethod.GET, value = { "/", "index.html" })
	public String toIndex(ModelAndView model) {
		Authentication  auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null && auth.isAuthenticated() && hasRoleUser(auth.getAuthorities())) {
			return "redirect:/home";
		}
		return "index";
	}

	@RequestMapping(method = RequestMethod.GET, value = { "/home" })
	public String toHome(ModelMap model) {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		model.addAttribute("userName", userName);
		return "home";
	}
	
	private boolean hasRoleUser(Collection<? extends GrantedAuthority> roles) {
		
		boolean result = false;
		
		if(roles != null && !roles.isEmpty()) {
			for(GrantedAuthority role : roles) {
				if("ROLE_USER".equals(role.getAuthority())) {
					result = true;
					break;
				}
			}
		}
		
		return result;
	}
}
