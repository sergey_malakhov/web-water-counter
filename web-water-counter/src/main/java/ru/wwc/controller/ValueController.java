package ru.wwc.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.wwc.domain.CounterValue;
import ru.wwc.domain.WaterCounter;
import ru.wwc.service.CounterValueService;
import ru.wwc.service.UserService;
import ru.wwc.service.WaterCounterService;

@Controller
@RequestMapping("/values")
public class ValueController {
	
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CounterValueService valueService;
	
	@Autowired
	private WaterCounterService counterServise;
	
	@RequestMapping(method=RequestMethod.GET)
	public String getValuesInfo(ModelMap model) {
		
		Authentication  auth = SecurityContextHolder.getContext().getAuthentication();
		String userLogin = auth.getName();
		
		
		List<WaterCounter> counters = counterServise.getUserCountersWithValues(userLogin);
		
		model.addAttribute("counters", counters);
		
		return "values";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{year}/{month}")
	public String getValuesInfo(@PathVariable Short year, @PathVariable Short month, ModelMap model) {
		
		Authentication  auth = SecurityContextHolder.getContext().getAuthentication();
		String userLogin = auth.getName();
		
		Map<WaterCounter, List<CounterValue>> counterValues = valueService.getValues(userLogin, year, month);
		
		model.addAttribute("counterValuesMap", counterValues);
		
		return "values";
	}
}
