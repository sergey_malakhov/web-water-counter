package ru.wwc.dao;

import ru.wwc.domain.CounterValue;

public interface CounterValueDao extends BaseDao<CounterValue> {

}
