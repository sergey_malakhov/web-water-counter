package ru.wwc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.wwc.dao.WaterCounterDao;
import ru.wwc.domain.WaterCounter;

@Repository
public class WaterCounterDaoImpl implements WaterCounterDao {

	@Autowired
	SessionFactory sessionFactory;	
	
	@Override
	public WaterCounter create(WaterCounter counter) {
		if (counter.getId() != null) {
			throw new IllegalArgumentException(
					"Идентификатор должен быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();

		ses.persist(counter);
		return counter;
	}

	@Override
	public void update(WaterCounter counter) {
		if (counter.getId() == null) {
			throw new IllegalArgumentException(
					"Идентификатор не должен быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();

		ses.merge(counter);		
	}

	@Override
	public void delete(Long id) {
		WaterCounter counter = find(id);
		if (counter != null) {
			sessionFactory.getCurrentSession().delete(counter);
		}
	}

	@Override
	public WaterCounter find(Long id) {
		if (id == null) {
			throw new IllegalArgumentException(
					"Идентификатор не должен быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();
		WaterCounter result = (WaterCounter) ses.get(WaterCounter.class, id);

		return result;
	}

	@Override
	public List<WaterCounter> findByUser(Long userId) {
		Session ses = sessionFactory.getCurrentSession();
		Query query = ses
				.createQuery("SELECT wc FROM WaterCounter wc INNER JOIN wc.user u WHERE u.id = :userId");
		query.setParameter("userId", userId);
				
		@SuppressWarnings("unchecked")
		List<WaterCounter> result = query.list();

		return result;
	}
	
	@Override
	public List<WaterCounter> findByUser(String userLogin) {
		Session ses = sessionFactory.getCurrentSession();
		Query query = ses
				.createQuery("SELECT wc FROM WaterCounter wc INNER JOIN wc.user u WHERE u.login = :userLogin");
		query.setParameter("userLogin", userLogin);
				
		@SuppressWarnings("unchecked")
		List<WaterCounter> result = query.list();
		return result;
	}
	
	@Override
	public List<WaterCounter> findByUserWithValues(String userLogin) {
		Session ses = sessionFactory.getCurrentSession();
		Query query = ses
				.createQuery("SELECT wc FROM WaterCounter wc INNER JOIN wc.user u LEFT JOIN FETCH wc.values WHERE u.login = :userLogin ORDER BY wc.place, wc.type");
		query.setParameter("userLogin", userLogin);
				
		@SuppressWarnings("unchecked")
		List<WaterCounter> result = query.list();
		
		return result;
	}

}
