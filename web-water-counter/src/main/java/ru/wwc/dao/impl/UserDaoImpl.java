package ru.wwc.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.wwc.dao.UserDao;
import ru.wwc.domain.User;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User create(User user) {
		if (user.getId() != null) {
			throw new IllegalArgumentException(
					"Идентификатор должен быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();

		ses.persist(user);
		return user;
	}

	@Override
	public void update(User user) {
		if (user.getId() == null) {
			throw new IllegalArgumentException(
					"Идентификатор не должен быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();

		ses.merge(user);
	}

	@Override
	public void delete(Long id) {
		User user = find(id);
		if (user != null) {
			sessionFactory.getCurrentSession().delete(user);
		}
	}

	@Override
	public User find(Long id) {
		if (id == null) {
			throw new IllegalArgumentException(
					"Идентификатор не должен быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();
		User result = (User) ses.get(User.class, id);

		return result;
	}

	@Override
	public User find(String login) {
		if (login == null || login.isEmpty()) {
			throw new IllegalArgumentException("Логин не должен быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();
		Query query = ses
				.createQuery("SELECT u FROM User u INNER JOIN u.roles WHERE u.login = :login");
		query.setParameter("login", login);
		User result = (User) query.list().get(0);

		return result;
	}

	@Override
	public boolean isUniqueLogin(String login) {
		if (login == null || login.isEmpty()) {
			throw new IllegalArgumentException("Логин не должен быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();
		Query query = ses
				.createQuery("SELECT u.login FROM User u WHERE u.login = :login");
		query.setParameter("login", login);
		return query.list().size() == 0;
	}

	@Override
	public boolean isUniqueEmail(String email) {
		if (email == null || email.isEmpty()) {
			throw new IllegalArgumentException("email не должен быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();
		Query query = ses
				.createQuery("SELECT u.login FROM User u WHERE u.email = :email");
		query.setParameter("email", email);
		return query.list().size() == 0;
	}
	
	
	
}
