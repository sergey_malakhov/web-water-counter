package ru.wwc.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.wwc.dao.RoleDao;
import ru.wwc.domain.Role;

@Repository
public class RoleDaoImpl implements RoleDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Role create(Role role) {
		if (role.getId() != null) {
			throw new IllegalArgumentException(
					"Идентификатор должен быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();

		ses.persist(role);

		return role;
	}

	@Override
	public void update(Role role) {
		if (role.getId() == null) {
			throw new IllegalArgumentException(
					"Идентификатор не должен быть пустым");
		}

		Session ses = sessionFactory.getCurrentSession();

		ses.merge(role);

	}

	@Override
	public void delete(Long id) {
		Role role = find(id);
		Session ses = sessionFactory.getCurrentSession();
		if (role != null) {
			ses.delete(role);
		}
	}

	@Override
	public Role find(Long id) {
		if (id == null) {
			throw new IllegalArgumentException(
					"Идентификатор не должен быть пустым");
		}

		Session ses = sessionFactory.getCurrentSession();

		Role result = (Role) ses.get(Role.class, id);

		return result;
	}

	@Override
	public Role find(String name) {
		if (name == null) {
			throw new IllegalArgumentException(
					"Наименование не должено быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();

		Query q = ses.createQuery("FROM Role r WHERE r.name = :name");
		q.setParameter("name", name);
		Role result = (Role) q.list().get(0);
		return result;
	}

}
