package ru.wwc.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.wwc.dao.CounterValueDao;
import ru.wwc.domain.CounterValue;

@Repository
public class CounterValueDaoImpl implements CounterValueDao {
	
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public CounterValue create(CounterValue obj) {
		if (obj.getId() != null) {
			throw new IllegalArgumentException(
					"Идентификатор должен быть пустым");
		}
		Session ses = sessionFactory.getCurrentSession();

		ses.persist(obj);

		return obj;
	}

	@Override
	public void update(CounterValue obj) {
		if (obj.getId() == null) {
			throw new IllegalArgumentException(
					"Идентификатор не должен быть пустым");
		}

		Session ses = sessionFactory.getCurrentSession();

		ses.merge(obj);
	}

	@Override
	public void delete(Long id) {
		CounterValue obj = find(id);
		Session ses = sessionFactory.getCurrentSession();
		if (obj != null) {
			ses.delete(obj);
		}
		
	}

	@Override
	public CounterValue find(Long id) {
		if (id == null) {
			throw new IllegalArgumentException(
					"Идентификатор не должен быть пустым");
		}

		Session ses = sessionFactory.getCurrentSession();

		CounterValue result = (CounterValue) ses.get(CounterValue.class, id);

		return result;
	}

	

}
