package ru.wwc.dao;

import ru.wwc.domain.LongIdEntity;

public interface BaseDao<T extends LongIdEntity> {
	
	T create(T obj);

	void update(T obj);

	void delete(Long id);

	T find(Long id);
}
