package ru.wwc.dao;

import java.util.List;

import ru.wwc.domain.WaterCounter;


public interface WaterCounterDao extends BaseDao<WaterCounter> {

	List<WaterCounter> findByUser(Long userId);
	
	List<WaterCounter> findByUser(String userLogin);
	
	List<WaterCounter> findByUserWithValues(String userLogin);

}