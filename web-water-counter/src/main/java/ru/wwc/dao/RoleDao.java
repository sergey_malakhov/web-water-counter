package ru.wwc.dao;

import ru.wwc.domain.Role;

public interface RoleDao extends BaseDao<Role> {

	Role find(String name);

}