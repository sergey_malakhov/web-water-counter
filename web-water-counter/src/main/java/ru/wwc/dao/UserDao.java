package ru.wwc.dao;

import ru.wwc.domain.User;

public interface UserDao extends BaseDao<User> {

	User find(String login);
	
	boolean isUniqueLogin(String login);
	
	boolean isUniqueEmail(String email);

}