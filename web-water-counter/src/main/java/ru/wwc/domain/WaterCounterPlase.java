package ru.wwc.domain;

public enum WaterCounterPlase {
	NO(""),
	BATH("Ванная комната"),
	WC("Туалет"),
	KITCHEN("Кухня");
	
	private String title;
	
	private WaterCounterPlase(String name) {
		this.title = name;
	}
	
	public String getTitle() {
		return title;
	}
}
