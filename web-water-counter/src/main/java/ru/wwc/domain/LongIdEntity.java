package ru.wwc.domain;

import java.io.Serializable;

public interface LongIdEntity extends Serializable {

	Long getId();

	void setId(Long id);

}
