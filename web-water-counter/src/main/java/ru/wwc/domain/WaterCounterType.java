package ru.wwc.domain;

public enum WaterCounterType {
	COLD("Счетчик холодной воды"),
	HOT("Счетчик горячей воды");
	
	private String title;
	
	private WaterCounterType(String name) {
		this.title = name;
	}
	
	public String getTitle() {
		return title;
	}
}
