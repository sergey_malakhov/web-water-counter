package ru.wwc.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Прибор учета, базовый класс. Инкапсулирует основную информацию по прибору.
 * 
 * @author sergey.malahov
 * 
 */

@Entity
@Table(name="WaterCounter")
public class WaterCounter implements LongIdEntity {
	
	private static final long serialVersionUID = 927651815488821898L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	@Column(name="pasportNumber", nullable=false, length=255)
	@Size(max=255)
	@NotBlank
	private String pasportNumber;
	@Enumerated(EnumType.STRING)
	@Column(name="place", nullable=false)
	@NotNull
	private WaterCounterPlase place;
	@Enumerated(EnumType.STRING)
	@Column(name="type", nullable=false)
	@NotNull
	private WaterCounterType type;
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	@ForeignKey(name="fk_watercounter_user_id")
	private User user;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinTable(name = "WaterCounter_Value", joinColumns = @JoinColumn(name = "counter_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "value_id", referencedColumnName = "id"))
	@ForeignKey(name = "fk_waterCounter_value_counter_id", inverseName = "fk_waterCounter_value_value_id")
	@OrderBy("date")
	private List<CounterValue> values;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getPasportNumber() {
		return pasportNumber;
	}

	public void setPasportNumber(String pasportNumber) {
		this.pasportNumber = pasportNumber;
	}

	public WaterCounterType getType() {
		return type;
	}

	public void setType(WaterCounterType type) {
		this.type = type;
	}

	public WaterCounterPlase getPlace() {
		return place;
	}

	public void setPlace(WaterCounterPlase place) {
		this.place = place;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<CounterValue> getValues() {
		return values;
	}

	public void setValues(List<CounterValue> values) {
		this.values = values;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((pasportNumber == null) ? 0 : pasportNumber.hashCode());
		result = prime
				* result
				+ ((place == null) ? 0 : place.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WaterCounter other = (WaterCounter) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (pasportNumber == null) {
			if (other.pasportNumber != null)
				return false;
		} else if (!pasportNumber.equals(other.pasportNumber))
			return false;
		if (place == null) {
			if (other.place != null)
				return false;
		} else if (!place.equals(other.place))
			return false;
		if (type != other.type)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

	

}
