package ru.wwc.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import ru.wwc.dao.CounterValueDao;
import ru.wwc.dao.UserDao;
import ru.wwc.dao.WaterCounterDao;
import ru.wwc.domain.CounterValue;
import ru.wwc.domain.WaterCounter;

@Service
public class CounterValueService {
	
	@Autowired
	private CounterValueDao valueDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private WaterCounterDao counterDao;
	
	@Transactional
	public void createWaterValue(CounterValue value, Long counterId, String userLogin) {
		
	}
	
	@Transactional(isolation=Isolation.READ_COMMITTED)
	public Map<WaterCounter, List<CounterValue>> getCurrentValues(String userLogin) {
		
		return new LinkedHashMap<WaterCounter, List<CounterValue>>();
	}

	@Transactional(isolation=Isolation.READ_COMMITTED)
	public Map<WaterCounter, List<CounterValue>> getValues(String userLogin, Short year, Short month) {
		
		return new LinkedHashMap<WaterCounter, List<CounterValue>>();
	}
	
}
