package ru.wwc.service;

import java.util.Collection;
import java.util.HashSet;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import ru.wwc.dao.RoleDao;
import ru.wwc.dao.UserDao;
import ru.wwc.domain.Role;
import ru.wwc.domain.User;
import ru.wwc.exception.domain.user.UserException;

@Service
public class UserService {


	@Autowired
	UserDao userDao;

	@Autowired
	RoleDao roleDao;

	@Autowired
	SessionFactory sf;

	@Transactional(isolation=Isolation.SERIALIZABLE)
	public User createUser(String login, String email, String password) {

		Role role = roleDao.find("ROLE_USER");
		User u = new User();
		u.setLogin(login);
		u.setEmail(email);
		ShaPasswordEncoder encoder = new ShaPasswordEncoder();
		u.setPwdHash(encoder.encodePassword(password, null));
		u.getRoles().add(role);

		User result = userDao.create(u);

		return result;
	}
	
	@Transactional(isolation=Isolation.SERIALIZABLE)
	public User createUser(User user) throws UserException {
		
		boolean uniqueLogin = userDao.isUniqueLogin(user.getLogin());
		boolean uniqueEmail = userDao.isUniqueEmail(user.getEmail());
		
		if(!uniqueEmail || !uniqueEmail) {
			UserException ex = new UserException();
			
			if(!uniqueLogin) {
				 ex.getErrorKeys().add(UserException.NON_UNIQUE_LOGIN);
			}
			if(!uniqueEmail) {
				ex.getErrorKeys().add(UserException.NON_UNIQUE_EMAIL);
			}
			throw ex;
		}

		Role role = roleDao.find("ROLE_USER");
		ShaPasswordEncoder encoder = new ShaPasswordEncoder();
		user.setPwdHash(encoder.encodePassword(user.getPwdHash(), null));
		user.getRoles().add(role);

		User result = userDao.create(user);

		return result;
	}

	public void authorizeUser(User user) {
		Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		for (Role role : user.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
		}
		Authentication auth = new UsernamePasswordAuthenticationToken(
				user.getLogin(), user.getPwdHash(), authorities);
		SecurityContextHolder.getContext().setAuthentication(auth);
	}

}
