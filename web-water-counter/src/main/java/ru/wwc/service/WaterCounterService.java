package ru.wwc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import ru.wwc.dao.UserDao;
import ru.wwc.dao.WaterCounterDao;
import ru.wwc.domain.User;
import ru.wwc.domain.WaterCounter;


@Service
public class WaterCounterService {

	@Autowired
	WaterCounterDao counterDao;
	
	@Autowired
	UserDao userDao;
	
	@Transactional(isolation=Isolation.READ_COMMITTED)
	public WaterCounter create(WaterCounter counter, String userLogin) {
		
		User currentUser = userDao.find(userLogin);
		counter.setUser(currentUser);
		
		WaterCounter result = counterDao.create(counter);
		return result;
	}
	
	@Transactional(isolation=Isolation.READ_COMMITTED)
	public void update(WaterCounter counter, String userLogin) {	
		User user = userDao.find(userLogin);
		counter.setUser(user);
		counterDao.update(counter);		
	}
	
	@Transactional(isolation=Isolation.READ_COMMITTED)
	public void delete(Long counterId, String userLogin) {	
		List<WaterCounter> userCounters = counterDao.findByUser(userLogin);
		WaterCounter currentCounter = null;
		for(WaterCounter counter : userCounters) {
			if(counter.getId().equals(counterId)) {
				currentCounter = counter;
				break;
			}
		}
		if(currentCounter != null) {
			counterDao.delete(currentCounter.getId());					
		} else {
			
		}
	}
	
	@Transactional(isolation=Isolation.READ_COMMITTED)
	public List<WaterCounter> getUserCounters(Long userId) {
		return counterDao.findByUser(userId);
	}
	
	@Transactional(isolation=Isolation.READ_COMMITTED)
	public List<WaterCounter> getUserCounters(String userLogin) {
		return counterDao.findByUser(userLogin);
	}
	
	@Transactional(isolation=Isolation.READ_COMMITTED)
	public List<WaterCounter> getUserCountersWithValues(String userLogin) {
		return counterDao.findByUserWithValues(userLogin);
	}

}
