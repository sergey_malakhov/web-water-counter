package ru.wwc.service;

import java.util.Collection;
import java.util.HashSet;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import ru.wwc.dao.UserDao;
import ru.wwc.domain.Role;

@Service
public class AuthUserDetailService implements UserDetailsService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	@Transactional(isolation=Isolation.READ_COMMITTED)
	public UserDetails loadUserByUsername(String login)
			throws UsernameNotFoundException {

		ru.wwc.domain.User u = userDao.find(login);

		if (u == null) {
			throw new UsernameNotFoundException("Пользователь с логином \""
					+ login + "\" не найден");
		}

		Collection<GrantedAuthority> roles = new HashSet<GrantedAuthority>();

		for (Role r : u.getRoles()) {
			roles.add(new SimpleGrantedAuthority(r.getName()));
		}

		UserDetails result = new User(u.getLogin(), u.getPwdHash(), roles);
		return result;

	}

}
