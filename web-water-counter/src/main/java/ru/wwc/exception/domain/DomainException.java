package ru.wwc.exception.domain;

import java.util.HashSet;
import java.util.Set;

public class DomainException extends Exception {

	private static final long serialVersionUID = 2495463430115209849L;
	
	private Set<String> errorKeys = new HashSet<String>();

	public DomainException() {
		// TODO Auto-generated constructor stub
	}

	public DomainException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DomainException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public DomainException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DomainException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
	
	public boolean hasErrors() {
		return !errorKeys.isEmpty();
	}
	
	public Set<String> getErrorKeys() {
		return errorKeys;
	}

}
