package ru.wwc.exception.domain.user;

import ru.wwc.exception.domain.DomainException;

public class UserException extends DomainException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4310102580191208825L;
	
	public static String NON_UNIQUE_LOGIN = "NON_UNIQUE_LOGIN";
	public static String NON_UNIQUE_EMAIL = "NON_UNIQUE_EMAIL";

	public UserException() {
		// TODO Auto-generated constructor stub
	}

	public UserException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public UserException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
