package ru.wwc.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ru.wwc.dao.UserDao;
import ru.wwc.dao.WaterCounterDao;
import ru.wwc.domain.User;
import ru.wwc.domain.WaterCounter;
import ru.wwc.domain.WaterCounterPlase;
import ru.wwc.domain.WaterCounterType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-context.xml")
public class WaterCounterDaoTest {
	
	private User currentUser;
	
	@Autowired
	private WaterCounterDao dao;
	
	@Autowired
	private UserDao userDao;
	
	@Before
	public void initUser() {
		User user = new User();
		user.setLogin("waterCounterTestUser");
		user.setPwdHash("waterCounterTestUser");
		user.setEmail("waterCounterTestUser@mail.ru");
		currentUser = userDao.create(user);
	}

	@Test
	@Transactional
	public void testCreate() {
		WaterCounter counter = new WaterCounter();
		counter.setPasportNumber("1");
		counter.setType(WaterCounterType.COLD);
		counter.setPlace(WaterCounterPlase.BATH);
		counter.setUser(currentUser);
		WaterCounter created = dao.create(counter);
		assertNotNull("created id is null", created.getId());
	}

	@Test
	@Transactional
	public void testUpdate() {
		WaterCounter counter = new WaterCounter();
		counter.setPasportNumber("2");
		counter.setType(WaterCounterType.COLD);
		counter.setPlace(WaterCounterPlase.BATH);
		counter.setUser(currentUser);
		WaterCounter cu = dao.create(counter);
		Long id = cu.getId();
		cu.setType(WaterCounterType.HOT);
		dao.update(cu);
		WaterCounter fu = dao.find(id);
		assertEquals("not update coldType to hotType", WaterCounterType.HOT, fu.getType());
	}

	@Test
	@Transactional
	public void testDelete() {
		WaterCounter counter = new WaterCounter();
		counter.setPasportNumber("3");
		counter.setType(WaterCounterType.COLD);
		counter.setPlace(WaterCounterPlase.BATH);
		counter.setUser(currentUser);
		WaterCounter cu = dao.create(counter);
		Long id = cu.getId();
		dao.delete(id);
		WaterCounter fu = dao.find(id);
		assertNull("find deleted", fu);
	}

	@Test
	@Transactional
	public void testFind() {
		WaterCounter counter = new WaterCounter();
		counter.setPasportNumber("4");
		counter.setType(WaterCounterType.COLD);
		counter.setPlace(WaterCounterPlase.BATH);
		counter.setUser(currentUser);
		WaterCounter cu = dao.create(counter);
		WaterCounter fu = dao.find(cu.getId());
		assertEquals("u != fu", cu, fu);		
	}
	
	@Test
	@Transactional
	public void testFindByUserId() {
		WaterCounter counter = new WaterCounter();
		counter.setPasportNumber("5");
		counter.setType(WaterCounterType.COLD);
		counter.setPlace(WaterCounterPlase.BATH);
		counter.setUser(currentUser);
		WaterCounter cu = dao.create(counter);
		List<WaterCounter> byUser = dao.findByUser(currentUser.getId());
		assertTrue("list not contain counter number 5", byUser.contains(cu));		
	}
	
	@Test
	@Transactional
	public void testFindByUserLogin() {
		WaterCounter counter = new WaterCounter();
		counter.setPasportNumber("6");
		counter.setType(WaterCounterType.COLD);
		counter.setPlace(WaterCounterPlase.BATH);
		counter.setUser(currentUser);
		WaterCounter cu = dao.create(counter);
		List<WaterCounter> byUser = dao.findByUser(currentUser.getLogin());
		assertTrue("list not contain counter number 5", byUser.contains(cu));		
	}

}
