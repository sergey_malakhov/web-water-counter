package ru.wwc.test.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ UserDaoTest.class, RoleDaoTest.class, WaterCounterDaoTest.class, CounterValueDaoTest.class })
public class DaoTests {	

}
