package ru.wwc.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ru.wwc.dao.CounterValueDao;
import ru.wwc.domain.CounterValue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-context.xml")
public class CounterValueDaoTest {
	
	@Autowired
	private CounterValueDao dao;

	@Test
	@Transactional
	public void testCreate() {
		CounterValue val = new CounterValue();
		val.setDate(new Date());
		val.setValue(BigDecimal.ZERO);
		CounterValue created = dao.create(val);
		assertNotNull("created id is null", created.getId());
	}

	@Test
	@Transactional
	public void testUpdate() {
		CounterValue v = new CounterValue();
		v.setDate(new Date());
		v.setValue(BigDecimal.ZERO);
		CounterValue ur = dao.create(v);
		Long id = ur.getId();
		ur.setValue(BigDecimal.TEN);
		dao.update(ur);
		CounterValue fr = dao.find(id);
		assertEquals("not update 0 to 10", BigDecimal.TEN, fr.getValue());
	}

	@Test
	@Transactional
	public void testDelete() {
		CounterValue v = new CounterValue();
		v.setDate(new Date());
		v.setValue(BigDecimal.ZERO);
		
		CounterValue cv = dao.create(v);
		Long id = cv.getId();
		dao.delete(id);
		CounterValue fv = dao.find(id);
		assertNull("find deleted", fv);
	}

	@Test
	@Transactional
	public void testFind() {
		CounterValue v = new CounterValue();
		v.setDate(new Date());
		v.setValue(BigDecimal.ZERO);
		v = dao.create(v);
		CounterValue fr = dao.find(v.getId());
		assertEquals("u != fu", v, fr);
	}

}
