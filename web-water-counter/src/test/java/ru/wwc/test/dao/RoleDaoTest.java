package ru.wwc.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import ru.wwc.dao.RoleDao;
import ru.wwc.domain.Role;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-context.xml")
public class RoleDaoTest {
	
	@Autowired
	private RoleDao dao;

	@Test
	@Transactional
	public void testCreate() {
		Role role = new Role();
		role.setName("create");
		role.setDescriprion("create");
		Role created = dao.create(role);
		assertNotNull("created id is null", created.getId());
	}

	@Test
	@Transactional
	public void testUpdate() {
		Role r = new Role();
		r.setName("update");
		r.setDescriprion("update");
		Role ur = dao.create(r);
		Long id = ur.getId();
		ur.setName("Update");
		dao.update(ur);
		Role fr = dao.find(id);
		assertEquals("not update update to Update", "Update", fr.getName());
	}

	@Test
	@Transactional
	public void testDelete() {
		Role r = new Role();
		r.setName("delete");
		r.setDescriprion("delete");
		
		Role cr = dao.create(r);
		Long id = cr.getId();
		dao.delete(id);
		Role fr = dao.find(id);
		assertNull("find deleted", fr);
	}

	@Test
	@Transactional
	public void testFind() {
		Role r = new Role();
		r.setName("find");
		r.setDescriprion("find");
		r = dao.create(r);
		Role fr = dao.find(r.getId());
		assertEquals("u != fu", r, fr);
		fr = dao.find(r.getName());
		assertEquals("u != fu", r, fr);
	}

}
